{if $_REQUEST['unit_id']}
    {assign var="id" value=$_REQUEST['unit_id']}
{else}
    {assign var="id" value=0}
{/if}
{$allow_save = $unit|fn_allow_save_object:"units"}
{$hide_inputs = ""|fn_check_form_permissions}
{assign var="b_type" value=$banner.type|default:"G"}
{capture name="mainbox"}
<form action="{""|fn_url}" method="post" class="form-horizontal form-edit{if !$allow_save || $hide_inputs} cm-hide-inputs{/if}" name="units_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="unit_id" value="{$id}" />
{capture name="tabsbox"}
        <div class="control-group">
            <label for="unit_name" class="control-label cm-required">{__("name")}</label>
            <div class="controls">
                <input type="text" name="unit_data[name]" id="unit_name" value="{$unit_data.name}" size="25" class="input-large cm-required" />
            </div>
        </div>
    <div id="content_general">
{hook name="banners:general_content"}
        <div class="control-group" id="banner_graphic">
            <label class="control-label">{__("image")}</label>
            <div class="controls">
                {include file="common/attach_images.tpl"
                    image_name="unit"
                    image_object_type="unit"
                    image_pair=$unit_data.main_pair
                    image_object_id=$id
                    no_detailed=true
                    hide_titles=true
                }
            </div>
        </div>
        <div class="control-group">
            <label for="unit_data[discription]" class="control-label">{__("description")}:</label>
            <div class="controls">
                <textarea id="discription" name="unit_data[discription]" cols="35" rows="8" class="cm-wysiwyg input-large">{$unit_data.discription}</textarea>
            </div>
        </div>
    <div class="control-group">
        <label class="control-label">{__("director")}</label>
        <div class="controls">
            {include 
            file="pickers/users/picker.tpl" 
            but_text=__("add_director") 
            data_id="return_users" 
            but_meta="btn" 
            input_name="unit_data[director]" 
            placement="right"
            display="radio"
            view_mode="single_button"
            user_info=$u_info
            item_ids=$unit_data.director 
            }
        </div>
    </div>
        <div class="control-group">
        <label class="control-label">{__("department_employees")}</label>
        <div class="controls">
            {include file="pickers/users/picker.tpl" 
            but_text=__("add_employees") 
            data_id="return_users" 
            but_meta="btn" 
            input_name="unit_data[workers]" 
            item_ids=$unit_data.workers 
            placement="right"}
        </div>
    </div>
    {if $_REQUEST['dispatch'] != "profiles.add_units"}
        <div class="control-group">
            <label for="create_time" class="control-label">{__("creation_date")}</label>
            <div class="controls">
                <input type="text" name="create_time" value="{$unit_data.create_time}" size="25" class="input-large" disabled/>
            </div>
        </div>
    {/if}
         {include file="common/select_status.tpl" input_name="unit_data[status]" id="unit_status" obj_id=$id obj=$unit_data hidden=false}
{/hook}
    <!--content_general--></div>
    <div id="content_addons" class="hidden clearfix">
        {hook name="banners:detailed_content"}
        {/hook}
    <!--content_addons-->
    </div>
    {hook name="banners:tabs_content"}
    {/hook}
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}
{capture name="buttons"}
    {if !$id}
        {include 
            file="buttons/save_cancel.tpl" 
            but_role="submit-link" 
            but_target_form="units_form" 
            but_name="dispatch[profiles.update_units]"}
    {else}
        {capture name="tools_list"}
            <li>{btn type="list" class="cm-confirm" text=__("delete") href="profiles.delete_units?unit_id=`$unit_data.unit_id`" method="POST"}</li>
            <li>{btn type="list" class="cm-confirm" text=__("save") href="profiles.update_units?unit_id=`$unit_data.unit_id`" method="POST"}</li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
        {if "ULTIMATE"|fn_allowed_for && !$allow_save}
            {assign var="hide_first_button" value=true}
            {assign var="hide_second_button" value=true}
        {/if}
            {include 
            file="buttons/save_cancel.tpl" 
            but_name="dispatch[profiles.update_units]" 
            but_role="submit-link" 
            but_target_form="units_form" 
            hide_first_button=$hide_first_button 
            hide_second_button=$hide_second_button 
            save=$id}
    {/if}
{/capture}
</form>
{/capture}
{if !$id}
    {$title = __("add_a_new_department")}
{else}
   {$title = __("change_the_department")}
{/if}
{include 
    file="common/mainbox.tpl"
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons
    select_languages=true
}