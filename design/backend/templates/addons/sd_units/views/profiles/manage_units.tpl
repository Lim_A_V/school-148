{capture name="mainbox"}
<form action="{""|fn_url}" method="post" id="units_form" name="units_form" enctype="multipart/form-data">
   <input type="hidden" name="fake" value="1" />
   {include 
        file="common/pagination.tpl"
        save_current_page=true 
        save_current_url=true 
        div_id="pagination_contents_units"
    }
   {$c_url=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
   {$rev=$smarty.request.content_id|default:"pagination_contents_units"}
   {$c_icon="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
   {$c_dummy="<i class=\"icon-dummy\"></i>"}
   {$banner_statuses=""|fn_get_default_statuses:true}
   {$has_permission = fn_check_permissions("units", "update_status", "admin", "POST")}
   {if $units}
        {capture name="units_table"}
        <div class="table-responsive-wrapper longtap-selection">
            <table class="table table-middle table--relative table-responsive">
            <thead>
            <tr>
                <th width="40%" class="left mobile-hide">
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}> {__("name")}
                      {if $search.sort_by == "name"}
                            {$c_icon nofilter}
                                {else}
                            {$c_dummy nofilter}
                      {/if}
                    </a>
                </th>
                <th width="30%" class="left mobile-hide">
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=logo&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("logo")}
                        {if $search.sort_by == "logo"}
                            {$c_icon nofilter}
                                {else}
                            {$c_dummy nofilter}
                        {/if}
                    </a>
                </th>
                <th width="10%" class="mobile-hide">
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=create_time&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("create_time")}
                        {if $search.sort_by == "create_time"}
                            {$c_icon nofilter}
                                {else}
                            {$c_dummy nofilter}
                        {/if}
                    </a>
                </th>
                <th width="10%"class="mobile-hide">&nbsp;</th>
                <th width="10%" class="right">
                    <a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}
                        {if $search.sort_by == "status"}
                            {$c_icon nofilter}
                                {else}
                            {$c_dummy nofilter}
                        {/if}
                    </a>
                </th>
            </tr>
            </thead>
            {foreach from=$units item=unit}
            <tr class="cm-row-status-{$unit.status|lower} cm-longtap-target"
                {if $has_permission}
                    data-ca-longtap-action="setCheckBox"
                    data-ca-longtap-target="input.cm-item"
                    data-ca-id="{$unit.unit_id}"
                {/if}>
                {$allow_save=true}
                {if $allow_save}
                    {$no_hide_input="cm-no-hide-input"}
                {else}
                    {$no_hide_input=""}
                {/if}
                <td class="{$no_hide_input}" data-th="{__("unit")}">
               {* <td class="left mobile-hide" > *}
                    <a class="row-status" href="{"profiles.update_units?unit_id={$unit.unit_id}"|fn_url}" class="product-title" title="{$unit.name}">{$unit.name}</a> 
                </td>
                <td class="{$no_hide_input}" data-th="{__("unit")}">
                        {include 
                                file="common/image.tpl" 
                                image=$unit.main_pair 
                                image_id=$product.main_pair.image_id 
                                image_width=100 
                                image_height=100
                                href="profiles.update_units?unit_id=`$unit.unit_id`"|fn_url
                                image_css_class="products-list__image--img"
                                link_css_class="products-list__image--link"
                        }
                </td>
                <td class="left mobile-hide"> {$unit.create_time}
                </td>
                <td class="mobile-hide">
                    {capture name="tools_list"}
                        <li>{btn type="list" text=__("edit") href="profiles.update_units?unit_id=`$unit.unit_id`"}</li>
                    {if $allow_save}
                        <li>{btn type="list" class="cm-confirm" text=__("delete") href="profiles.delete_units?unit_id=`$unit.unit_id`" method="POST"}</li>
                    {/if}
                    {/capture}
                    <div class="hidden-tools">
                        {dropdown content=$smarty.capture.tools_list}
                    </div>
                </td>
                    <!--статус отдела-->
                <td class="right" data-th="{__("status")}">
                    {include 
                        file="common/select_popup.tpl"
                        id=$unit.unit_id
                        status=$unit.status
                        hidden=true
                        object_id_name="unit_id"
                        table="units"
                        popup_additional_class="`$no_hide_input` dropleft"
                    }
                </td>
            </tr>
            {/foreach}
            </table>
        </div>
    {/capture}
    {include file="common/context_menu_wrapper.tpl"
        form="units_form"
        object="units"
        items=$smarty.capture.units_table
        has_permissions=$has_permission
    }
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{include 
    file="common/pagination.tpl" 
    div_id="pagination_contents_units"
    }
{capture name="adv_buttons"}
    {hook name="banners:adv_buttons"}
    {include 
        file="common/tools.tpl" 
        tool_href="profiles.add_units" 
        prefix="top" 
        hide_tools="true" 
        title=__("add_unit")
        icon="icon-plus"}
    {/hook}
{/capture}
</form>
{/capture}
{hook name="units:manage_mainbox_params"}
    {$page_title = __("units")}
    {$select_languages = true}
{/hook}
{capture name="sidebar"}
    {hook name="banners:manage_sidebar"}
    {include 
        file="common/saved_search.tpl" 
        dispatch="profiles.manage_units" 
        view_type="units"
    }
    {include 
        file="addons/sd_units/views/profiles/units_search_form.tpl" 
        dispatch="profiles.manage_units"
    }
    {/hook}
{/capture}
{include 
    file="common/mainbox.tpl" 
    title=$page_title 
    content=$smarty.capture.mainbox 
    adv_buttons=$smarty.capture.adv_buttons 
    sidebar=$smarty.capture.sidebar
    select_languages=$select_languages
    }
