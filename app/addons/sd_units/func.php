<?php

use Tygh\Languages\Languages;
use Tygh\Registry;

function fn_get_update_units($unit_id = 0, $lang_code = CART_LANGUAGE)
{
    $units = [];
    if (!empty($unit_id)) {
        list($units) = fn_get_units([
            'unit_id' => $unit_id
        ], 1, $lang_code);
        $units = !empty($units) ? reset ($units) : [];
    }
    return $units;
}
function fn_get_units($params = [], $items_per_page = 0, $lang_code = CART_LANGUAGE)
{
// Set default values to input params
$default_params = array(
    'page' => 1,
    'items_per_page' => $items_per_page
);
$params = array_merge($default_params, $params);
$sortings = array(
    'name' => '?:units_discription.name',
    'create_time' => '?:units.create_time',
    'status' => '?:units.status',
);
$condition = $limit = $join = '';
if (!empty($params['limit'])) {
    $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
}
$sorting = db_sort($params, $sortings, 'name', 'asc');
if (!empty($params['item_ids'])) {
    $condition .= db_quote(' AND ?:units.unit_id IN (?n)', explode(',', $params['item_ids']));
}
if (!empty($params['unit_id'])) {
    $condition .= db_quote(' AND ?:units.unit_id = ?i', $params['unit_id']);
}
if (!empty($params['status'])) {
    $condition .= db_quote(' AND ?:units.status = ?s', $params['status']);
}
if (!empty($params['name'])) {
    $condition .= db_quote(' AND ?:units_discription.name = ?s', $params['name']);
}
$fields = array (
    '?:units.unit_id',
    '?:units.create_time',
    '?:units.status',
    '?:units.logo',
    '?:units_discription.discription',
    '?:units_discription.name',
    '?:units_workers.director',
    '?:units_workers.workers',
);
$join .= db_quote(' LEFT JOIN ?:units_discription ON ?:units_discription.unit_id = ?:units.unit_id AND ?:units_discription.lang_code = ?s', $lang_code);
$join .= db_quote(' LEFT JOIN ?:units_workers ON ?:units_workers.unit_id = ?:units.unit_id AND ?:units_discription.lang_code = ?s', $lang_code);
if (!empty($params['items_per_page'])) {
    $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:units $join WHERE 1 $condition");
    $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
}
$units = db_get_hash_array(
    "SELECT ?p FROM ?:units " .
    $join .
    "WHERE 1 ?p ?p ?p",
    'unit_id', implode(', ', $fields), $condition, $sorting, $limit
);
$unit_image_ids = array_keys($units);
$images = fn_get_image_pairs($unit_image_ids, 'unit', 'M', true, false, $lang_code);
foreach ($units as $unit_id => $unit) {
   $units[$unit_id]['main_pair'] = !empty($images[$unit_id]) ? reset($images[$unit_id]) : array();
}
return array($units, $params);
}
function fn_update_unit($data, $unit_id, $lang_code = DESCR_SL)
{
    if (!empty($unit_id)) {
        db_query("UPDATE ?:units SET ?u WHERE unit_id = ?i", $data, $unit_id);
        db_query("UPDATE ?:units_discription SET ?u WHERE unit_id = ?i AND lang_code = ?s", $data, $unit_id, $lang_code);
        db_query("UPDATE ?:units_workers SET ?u WHERE unit_id = ?i", $data, $unit_id);
        
    } else {
        $data['create_time'] = date("d.m.Y");
        $unit_id = $data['unit_id'] = db_replace_into('units', $data);
        foreach (Languages::getAll() as $data['lang_code'] => $v) {
            db_query("REPLACE INTO ?:units ?e", $data);
            db_query("REPLACE INTO ?:units_discription ?e", $data);
            db_query("REPLACE INTO ?:units_workers ?e", $data);
        }
   }
   $pair_data = fn_attach_image_pairs('unit', 'unit', $unit_id, $lang_code);
   $data['logo'] = $pair_data[0];
    return $unit_id;
}