<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

$auth = & Tygh::$app['session']['auth'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') 
{
        // Define trusted variables that shouldn't be stripped
      //  fn_trusted_vars('unit_data');

if  ($mode == 'update_units') {

    $unit_id  = !empty($_REQUEST['unit_id']) ? $_REQUEST['unit_id'] : 0;
    $data = !empty($_REQUEST['unit_data']) ? $_REQUEST['unit_data'] : [];
    if ($unit_id != "0") {
        $suffix = ".update_units?unit_id={$unit_id}";
    } else {
        $suffix = ".manage_units";
    }
    $unit_id = fn_update_unit($data, $unit_id);
    return array(CONTROLLER_STATUS_OK, 'profiles' . $suffix);
 } 
} elseif ($mode == 'add_units' || $mode == 'update_units') {
    $unit_id = !empty($_REQUEST['unit_id']) ? $_REQUEST['unit_id'] : 0;
    $unit_data = fn_get_update_units($unit_id, DESCR_SL);
    if (empty($unit_data) && $mode == 'update_units') {
        return array(CONTROLLER_STATUS_NO_PAGE);
        }
     Tygh::$app['view']->assign([
        'unit_data' => $unit_data,
        'u_info' => !empty($unit_data['director']) ? fn_get_user_short_info($unit_data['director']) : [],
    ]);
} elseif ($mode == 'manage_units') {
    list($units, $search) = fn_get_units($_REQUEST, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
    Tygh::$app['view']->assign('units', $units);
    Tygh::$app['view']->assign('search', $search);
}
elseif ($mode == 'delete_units') {
        if (!empty($_REQUEST['unit_id'])) {
         db_query("DELETE FROM ?:units WHERE unit_id = ?i", $_REQUEST['unit_id']);
         db_query("DELETE FROM ?:units_discription WHERE unit_id = ?i", $_REQUEST['unit_id']);
         db_query("DELETE FROM ?:units_workers WHERE unit_id = ?i", $_REQUEST['unit_id']);
        }
    $suffix = ".manage_units";
    return array(CONTROLLER_STATUS_OK, 'profiles' . $suffix);
} 